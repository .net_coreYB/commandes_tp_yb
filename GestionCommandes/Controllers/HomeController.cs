﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using GestionCommandes.Models;

namespace GestionCommandes.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly MonContext _context;

        public HomeController(ILogger<HomeController> logger, MonContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            Bags();
            return View();
        }

        public IActionResult Privacy()
        {
            Bags();
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            Bags();
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public void Bags()
        {
            string A = _context.Clients.ToList().Count().ToString();
            ViewBag.A = A;
            string B = _context.Commandes.ToList().Count().ToString();
            ViewBag.B = B;
            string C = _context.Produits.ToList().Count().ToString();
            ViewBag.C = C;
            string D = _context.Categories.ToList().Count().ToString();
            ViewBag.D = D;
        }
    }
}
