﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GestionCommandes.Models
{
    public class Facture
    {
        public int Id { get; set; }
        public DateTime Date_Facture { get; set; }
        public Commande Commande { get; set; }
        public int CommandeId { get; set; }
    }
}
