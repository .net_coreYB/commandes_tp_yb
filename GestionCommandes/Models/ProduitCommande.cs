﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GestionCommandes.Models
{
    public class ProduitCommande
    {
        public int Id { get; set; }
        public Produit Produit { get; set; }
        public int ProduitId { get; set; }
        public Commande Commande { get; set; }
        public int CommandeId { get; set; }
    }
}
