﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GestionCommandes.Models
{
    public class MonContext : DbContext
    {
        public DbSet<Category> Categories{ get; set; }
        public DbSet<Client> Clients{ get; set; }
        public DbSet<Commande> Commandes{ get; set; }
        public DbSet<Facture> Factures{ get; set; }
        public DbSet<Produit> Produits{ get; set; }
        public DbSet<ProduitCommande>ProduitCommandes{ get; set; }

        public MonContext( ): base(SqlServerDbContextOptionsExtensions.UseSqlServer(new DbContextOptionsBuilder(),@"Data Source=.\SQLEXPRESS;initial catalog=GestionCommandes;integrated security=true;").Options)
        {

        }
}
}
