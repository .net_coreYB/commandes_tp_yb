﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GestionCommandes.Models
{
    public class Commande
    {
        public int Id { get; set; }
        public DateTime Date_Commande { get; set; }
        public List<ProduitCommande> ProduitCommandes { get; set; }
        public Facture Facture { get; set; }
        //public int FactureId { get; set; }
        public Client Client { get; set; }
        public int ClientId { get; set; }
}
}
