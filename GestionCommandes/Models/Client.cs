﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GestionCommandes.Models
{
    public class Client
    {
        public int Id { get; set; }
        public string Libelle { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Telephone { get; set; }
        public List<Commande> Commandes { get; set; }
    }
}
